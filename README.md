This repo contains the materials for reproducing the analysis in the paper "She Must Be Seeing Things! Gender disparity in camera department networks" by Pete Jones, Deb Verhoeven, Aresh Dadlani and Vejune Zemaityte (DOI : [10.1016/j.socnet.2023.09.004](https://doi.org/10.1016/j.socnet.2023.09.004)).

Data have been anonymised to prevent identifiability of participants; [renv](https://rstudio.github.io/renv/articles/renv.html) is used for ensuring a reproducible package environment in R and [Quarto](https://quarto.org/) is used to illustrate how to reproduce outputs and figures.
